﻿/* 
 * SQL Server Script
 * 
 * In a local environment (for example, with the SQLServerExpress instance 
 * included in the VStudio installation) it will be necessary to create the 
 * database and the user required by the connection string. So, the following
 * steps are needed:
 *
 *     Configure within the CREATE DATABASE sql-sentence the path where 
 *     database and log files will be created  
 *
 * This script can be executed from MS Sql Server Management Studio Express,
 * but also it is possible to use a command Line syntax:
 *
 *    > sqlcmd.exe -U [user] -P [password] -I -i SqlServerCreateTables.sql
 *
 */


USE [master]
GO

/****** Drop database if already exists  ******/
IF  EXISTS (SELECT name FROM sys.databases WHERE name = 'miniportal')
DROP DATABASE [miniportal]
GO

USE [master]
GO

/* DataBase Creation */
                                  

CREATE DATABASE [miniportal] 
   ON  
	 PRIMARY ( NAME = 'miniportal', FILENAME = 'D:\SourceCode\Database\miniportal.mdf') 
	 LOG ON ( NAME = 'miniportal_log', FILENAME = 'D:\SourceCode\Database\miniportal_log.ldf')
GO

